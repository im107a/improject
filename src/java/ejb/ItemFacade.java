/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Item;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class ItemFacade extends AbstractFacade<Item>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public ItemFacade() {
        super(Item.class);
    }
    
    public Item find(Integer id){
        Item item = em.find(Item.class, id);
        return item;
    }
    
    public List<Item> findByKeyword(String keyword){
        Query query = em.createQuery("SELECT i FROM Item i WHERE i.name LIKE :keyword", Item.class).setParameter("keyword", "%" + keyword + "%");
        return query.getResultList();
    }
    
    public List<Item> findByRestaurant(int supplierNo){
        Query query = em.createQuery("SELECT i FROM Item i WHERE i.supplier.supplierNo = :supplierNo", Item.class).setParameter("supplierNo", supplierNo);
        return query.getResultList();
    }
    
    public List<Item> findByKeywordAndRestaurant(String keyword, int supplierNo){
        Query query = em.createQuery("SELECT i FROM Item i WHERE i.supplier.supplierNo = :supplierNo AND i.name LIKE :keyword", Item.class).setParameter("supplierNo", supplierNo).setParameter("keyword", "%" + keyword + "%");
        return query.getResultList();
    }
    
    public int createItem(Item item){
        
        // persistence scope starts
        em.persist(item); 

        // return a detached entity; persistence scope ends
        return item.getId();  
    }
    
    public int updateItem(Integer id,Item item){
        em.find(Item.class, id);

        em.merge(item);

        // auto commit after leaving the method; persistence scope ends
        return item.getId();  
    }
    
    /**
    * Delete by item.
    * @param item 
    */
    public void delete(Item item){
       Item mergedItem = em.find(Item.class, item.getId());
       em.remove(mergedItem);
   }

   /**
    * Delete by Item ID
    * @param id 
    */
    public void delete(Integer id){
       Item mergedItem = em.find(Item.class, id);
       em.remove(mergedItem);
   }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
