/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.SalesOrder;
import entities.WorkOrder;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class SalesOrderFacade extends AbstractFacade<SalesOrder>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public SalesOrderFacade() {
        super(SalesOrder.class);
    }
    
    public SalesOrder find(Long id){
        SalesOrder salesOrder = em.find(SalesOrder.class, id);
        return salesOrder;
    }
    
    public List<SalesOrder> findByDESC(){
        Query query = em.createQuery("SELECT s FROM SalesOrder s ORDER BY s.orderNo DESC", SalesOrder.class);
        return query.getResultList();
    }
    
    public List<SalesOrder> findByStudentName(String name){
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.customerName = :name ORDER BY s.orderNo DESC", SalesOrder.class).setParameter("name", name);
        return query.getResultList();
    }
    
    public List<SalesOrder> findByState(String state){
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.salesOrderState = :state ORDER BY s.orderNo DESC", SalesOrder.class).setParameter("state", state);
        return query.getResultList();
    }
    
    public List<SalesOrder> findByStudentNameAndState(String name, String state){
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.customerName = :name AND s.salesOrderState = :state ORDER BY s.orderNo DESC", SalesOrder.class).setParameter("state", state).setParameter("name", name);
        return query.getResultList();
    }
    
    public List<SalesOrder> findByEmail(String email){
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.email = :email ORDER BY s.orderNo DESC", SalesOrder.class).setParameter("email", email);
        return query.getResultList();
    }
    
    public List<SalesOrder> findByStudentEmailAndState(String email, String state){
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.email = :email AND s.salesOrderState = :state ORDER BY s.orderNo DESC", SalesOrder.class).setParameter("state", state).setParameter("email", email);
        return query.getResultList();
    }
    
    public SalesOrder findByWorkOrderNo(Long workOrderNo){
        workOrderNo = workOrderNo/100;
        Query query = em.createQuery("SELECT s FROM SalesOrder s WHERE s.orderNo = :workOrderNo", SalesOrder.class).setParameter("workOrderNo", workOrderNo);
        return (SalesOrder)query.getSingleResult();
    }
    
    public Object lastOrderNo(){
        Query query = em.createQuery("SELECT MAX(s.orderNo) FROM SalesOrder s", SalesOrder.class);
        return query.getSingleResult();
    }
    
    public long createSalesOrder(SalesOrder salesOrder){
        // persistence scope starts
        em.persist(salesOrder); 

        // return a detached entity; persistence scope ends
        return salesOrder.getOrderNo();
    }
    
    public Long updateSalesOrder(Long id,SalesOrder salesOrder){
        em.find(WorkOrder.class, id);

        em.merge(salesOrder);

        // auto commit after leaving the method; persistence scope ends
        return salesOrder.getOrderNo();  
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
