/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Student;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class StudentFacade extends AbstractFacade<Student>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public StudentFacade() {
        super(Student.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
