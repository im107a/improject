/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Supplier;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class SupplierFacade extends AbstractFacade<Supplier>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public SupplierFacade() {
        super(Supplier.class);
    }
    public Supplier find(Integer id){
        Supplier supplier = em.find(Supplier.class, id);
        return supplier;
    }
    
    public Supplier findByName(String name){
        Query query = em.createQuery("SELECT s FROM Supplier s WHERE s.name = :name", Supplier.class).setParameter("name", name);
        return (Supplier)query.getSingleResult();
    }
    
    /**
    * Delete by item.
    * @param supplier 
    */
    public void delete(Supplier supplier){
       Supplier mergedSupplier = em.find(Supplier.class, supplier.getSupplierNo());
       em.remove(mergedSupplier);
   }

   /**
    * Delete by Item ID
    * @param id 
    */
    public void delete(Integer id){
       Supplier mergedItem = em.find(Supplier.class, id);
       em.remove(mergedItem);
   }
   
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
