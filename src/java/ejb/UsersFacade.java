/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Users;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class UsersFacade extends AbstractFacade<Users>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public UsersFacade() {
        super(Users.class);
    }
    
    public Users findByUserId(String UserId){
        Query query = em.createQuery("SELECT u FROM Users u WHERE u.userId = :UserId", Users.class).setParameter("UserId", UserId);
        return (Users)query.getSingleResult();
    }
    
    public boolean userIdExist(Users user1){
        Users user2 = em.find(Users.class, user1.getId());
        if(user2 == null)
            return false;
        else
            return true;
    }
    
    public boolean userNameExist(Users user){
        Query query = em.createQuery("SELECT u FROM Users u WHERE u.student.name = :studentName", Users.class).setParameter("studentName", user.getStudent().getName());
        if(query.getSingleResult() == null)
            return false;
        else
            return true;
    }
    
    public boolean userEmailExist(Users user){
        Query query = em.createQuery("SELECT u FROM Users u WHERE u.student.email = :studentEmail", Users.class).setParameter("studentEmail", user.getStudent().getEmail());
        if(query.getResultList().isEmpty())
            return false;
        else
            return true;
    }
    
    public String createUsers(Users users){
        
        // persistence scope starts
        em.persist(users); 

        // return a detached entity; persistence scope ends
        return users.getId();  
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
