/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.Users_Groups;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class Users_GroupsFacade extends AbstractFacade<Users_Groups>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public String createUsers_Groups(Users_Groups users_Groups){
        
        // persistence scope starts
        em.persist(users_Groups); 

        // return a detached entity; persistence scope ends
        return users_Groups.getGroupId();  
    }
    
    public Users_GroupsFacade() {
        super(Users_Groups.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
