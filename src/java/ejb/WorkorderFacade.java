/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejb;

import entities.WorkOrder;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author a1999
 */
@Stateless
@LocalBean
public class WorkorderFacade extends AbstractFacade<WorkOrder>{

    @PersistenceContext(name = "improjectPU")
    private EntityManager em;

    public WorkorderFacade() {
        super(WorkOrder.class);
    }
    
    public WorkOrder find(Long id){
        WorkOrder workOrder = em.find(WorkOrder.class, id);
        return workOrder;
    }
    
    public List<WorkOrder> findByDESC(){
        Query query = em.createQuery("SELECT w FROM WorkOrder w ORDER BY w.workOrderNo DESC", WorkOrder.class);
        return query.getResultList();
    }
    
    public List<WorkOrder> findBySupplier(Integer supplierNO){
        Query query = em.createQuery("SELECT w FROM WorkOrder w WHERE w.supplier.supplierNo = :supplierNO ORDER BY w.workOrderNo DESC", WorkOrder.class);
        query.setParameter("supplierNO", supplierNO);
        return query.getResultList();
    }
    
    public List<WorkOrder> findByState( String state){
        Query query = em.createQuery("SELECT w FROM WorkOrder w WHERE w.state = :state ORDER BY w.workOrderNo DESC", WorkOrder.class);
        query.setParameter("state", state);
        return query.getResultList();
    }
    
    public List<WorkOrder> findBySupplierAndState(Integer supplierNO, String state){
        Query query = em.createQuery("SELECT w FROM WorkOrder w WHERE w.supplier.supplierNo = :supplierNO AND w.state = :state ORDER BY w.workOrderNo DESC", WorkOrder.class);
        query.setParameter("supplierNO", supplierNO);
        query.setParameter("state", state);
        return query.getResultList();
    }

    public Long updateWorkOrder(Long id,WorkOrder workOrder){
        em.find(WorkOrder.class, id);

        em.merge(workOrder);

        // auto commit after leaving the method; persistence scope ends
        return workOrder.getWorkOrderNo();  
    }
    
    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
