/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author a1999
 */
@Entity
public class Supplier implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer supplierNo;
    
    private String name;
    
    private int location;
    
    private String introduction;
    
    public Supplier(){
        
    }
    
    public Supplier(int supplierNo,String name,int location){
        this.supplierNo=supplierNo;
        this.name=name;
        this.location=location;
    }
    
    public Integer getSupplierNo() {
        return supplierNo;
    }

    public void setSupplierNo(Integer supplierNo) {
        this.supplierNo = supplierNo;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public int getLocation() {
        return location;
    }

    public void setLocation(int location) {
        this.location = location;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (supplierNo != null ? supplierNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Supplier)) {
            return false;
        }
        Supplier other = (Supplier) object;
        if ((this.supplierNo == null && other.supplierNo != null) || (this.supplierNo != null && !this.supplierNo.equals(other.supplierNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitiesV2.Supplier[ id=" + supplierNo + " ]";
    }
    
}
