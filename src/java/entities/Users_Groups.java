/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

/**
 *
 * @author a1999
 */
@IdClass(UsersGroupsKey.class)
@Entity
public class Users_Groups implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    private String groupId;
    
    @Id
    private String userId;

    public Users_Groups(){
    }
    
    public Users_Groups(String groupId, String userId){
        this.groupId = groupId;
        this.userId = userId;
    }
    
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (groupId != null ? groupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Users_Groups)) {
            return false;
        }
        Users_Groups other = (Users_Groups) object;
        if ((this.groupId == null && other.groupId != null) || (this.groupId != null && !this.groupId.equals(other.groupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Users_Groups[ id=" + groupId + " ]";
    }
    
}
