/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

/**
 *
 * @author a1999
 */
@Entity
public class WorkOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long workOrderNo;
    
    private String orderTime;
    
    private String customerName;
    
    private String phoneNumber;
    
    private int workOrderTotal;
    
    private Supplier supplier;
    
    @OneToMany(cascade = CascadeType.ALL) 
    @JoinColumn(name = "WORKORDER_WORKORDERNO", referencedColumnName = "WORKORDERNO")
    private List<WorkOrderLine> lines;
    
    private String state = "備餐中";
    
    private String finishMealTime;

    public Long getWorkOrderNo() {
        return workOrderNo;
    }

    public void setWorkOrderNo(Long workOrderNo) {
        this.workOrderNo = workOrderNo;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getWorkOrderTotal() {
        return workOrderTotal;
    }

    public void setWorkOrderTotal(int workOrderTotal) {
        this.workOrderTotal = workOrderTotal;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public List<WorkOrderLine> getLines() {
        return lines;
    }

    public void setLines(List<WorkOrderLine> lines) {
        this.lines = lines;
    }

    public String getMealState() {
        return state;
    }

    public void setMealState(String mealState) {
        this.state = mealState;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFinishMealTime() {
        return finishMealTime;
    }

    public void setFinishMealTime(String finishMealTime) {
        this.finishMealTime = finishMealTime;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (workOrderNo != null ? workOrderNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WorkOrder)) {
            return false;
        }
        WorkOrder other = (WorkOrder) object;
        if ((this.workOrderNo == null && other.workOrderNo != null) || (this.workOrderNo != null && !this.workOrderNo.equals(other.workOrderNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entitiesV2.WorkOrder[ workOrderNo=" + workOrderNo + " ]";
    }
    
}
