/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 *
 * @author erus
 */
@Named(value = "messagesBean")
@SessionScoped
public class MessagesBean implements Serializable {

    private final String registrationToken = "dlfDMOZ3BHA:APA91bFh8pydHBfdbxC6xUQsCXvK0XZgLaD-mLanv11ImehNP6k9s08gRiXAEZJlUTkEaSbxpKZmEkiz96fYc23O6X-ay82lVkl-VYDIn77PV1WWlzxEuHrL2IyvOuqjMQX9lqaH_2Rr";
    private FirebaseApp firebaseAppInst;

    /**
     * Creates a new instance of messagesBean
     */
    public MessagesBean() {
    }

    @PostConstruct
    public void initBean() {
        System.out.println("Init MessagesBean");
        try {
            this.initFirebase();
        } catch (IOException e) {
            Logger.getLogger(MessagesBean.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private InputStream openServiceAccountFile() {
        final String SERVICE_ACCOUNT_FILE = "/WEB-INF/service-account-file.json";
        InputStream serviceAccountFileStream = null;
        serviceAccountFileStream = FacesContext.getCurrentInstance()
                .getExternalContext()
                .getResourceAsStream(SERVICE_ACCOUNT_FILE);

        return serviceAccountFileStream;
    }
    
     private InputStream openServiceAccountFileForUnitTest() {
        final String SERVICE_ACCOUNT_FILE = "./build/web/WEB-INF/service-account-file.json";
        InputStream serviceAccountFileStream = null;
        try {
            serviceAccountFileStream = new FileInputStream(SERVICE_ACCOUNT_FILE);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MessagesBean.class.getName()).log(Level.SEVERE, null, ex);
        }

        return serviceAccountFileStream;
    }
    
/**
 * 初始化 Firebase Application
 * @return Ture 表示執行初始化動作; False 表示沒有執行, 因為先前己經初始化完畢。
 * @throws IOException 
 */
    public boolean initFirebase() throws IOException {
        
        //鑑別是否初始化
        if (!FirebaseApp.getApps().isEmpty()) {
            this.firebaseAppInst = FirebaseApp.getInstance();
            return false;
        } else {
            InputStream serviceAccount = this.openServiceAccountFile();
//            InputStream serviceAccount = this.openServiceAccountFileForUnitTest();
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://improject-46566.firebaseio.com")
                    .build();
            FirebaseApp.initializeApp(options);
            return true;
        }
    }

    public String notifyMealReady() throws FirebaseMessagingException {
        // See documentation on defining a message payload.
        LocalDateTime currentDateTime = LocalDateTime.now();
        String currentDateTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(currentDateTime);
        Message message = Message.builder()
                .setNotification(new Notification("系統通知", "你的餐點狀態"))
                .putData("time", currentDateTimeStr)
                .putData("state", "待取餐")
                .setToken(registrationToken)
                .build();

        // Send a message to the device corresponding to the provided
        // registration token.
        String response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);
        return response;
    }

    public String messaging(String stateStr) throws FirebaseMessagingException {
        String response = null;
        LocalDateTime currentDateTime = LocalDateTime.now();
        String currentDateTimeStr = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(currentDateTime);
        Message message = Message.builder()
                .setNotification(new Notification("系統通知", "你的餐點狀態"))
                .putData("time", currentDateTimeStr)
                .putData("state", stateStr)
                .setToken(registrationToken)
                .build();

        // Send a message to the device corresponding to the provided
        // registration token.
        response = FirebaseMessaging.getInstance().send(message);
        // Response is a message ID string.
        System.out.println("Successfully sent message: " + response);
        return response;
    }

    public String notifyCloseWorkOrder() throws FirebaseMessagingException {
        String response = this.messaging("工單已完成");
        return response;
    }

}
