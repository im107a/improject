/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.ItemFacade;
import ejb.UsersFacade;
import entities.Item;
import entities.Users;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import security.AuthBean;
import util.order.MessageHelper;


/**  
 * used facelet page : customerInformation.xhtml produceOrder.xhtml
 * @author a19990366@gmail.com
 */
@Named(value = "cartBean")
@SessionScoped
public class CartBean implements Serializable {
    private List<Item> shoppingCart;
    private List<Item> randomSelect;
    private int cart_total;
    private Item randomItem;
    
    @EJB
    private UsersFacade usersPool;
    @EJB
    private ItemFacade mealPool;
    
    @Inject
    private OrderProcessingBean orderProcessingBean;
    @Inject
    private AuthBean authBean;
    
    public CartBean() {
    }
   
    @PostConstruct
    public void init(){
        shoppingCart = new ArrayList<>();
    }

    public List<Item> getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(List<Item> carts) {
        this.shoppingCart = carts;
    }

    public int getCart_total() {
        return cart_total;
    }

    public void setCart_total(int cart_total) {
        this.cart_total = cart_total;
    }

    public List<Item> getRandomSelect() {
        return randomSelect;
    }

    public void setRandomSelect(List<Item> randomSelect) {
        this.randomSelect = randomSelect;
    }

    public ItemFacade getMealPool() {
        return mealPool;
    }

    public void setMealPool(ItemFacade mealPool) {
        this.mealPool = mealPool;
    }

    public Item getRandomItem() {
        return randomItem;
    }

    public void setRandomItem(Item randomItem) {
        this.randomItem = randomItem;
    }
    
    public void addCart(Item item){
        if(!shoppingCart.contains(item)){
            item.setQuantity(1);
            shoppingCart.add(item);
            MessageHelper.addMessage("餐點成功加入到購物車");
        }
        else{
            MessageHelper.addMessage("購物車已經有此餐點");
        }
    }
    
    public String removeCart(Item item){
        shoppingCart.remove(item);
        MessageHelper.addMessage("已刪除餐點");
        return "/student/shoppingCart?faces-redirect=true";
    }
    
    public String randomFind(){
        randomSelect = mealPool.findAll();
        Random random = new Random();
        int length = randomSelect.size();
        int num = random.nextInt(length);
        Item choice = randomSelect.get(num);
        randomItem = choice;
        return "/student/randomOrder?faces-redirect=true";
    }
    
    public String adjustQty(Item item, int amount){
        int currentQty = item.getQuantity();
        if((currentQty+amount)>=1){
            item.setQuantity(currentQty + amount);
        }
        return null;
    }
    
    public int subTotal(Item item){      
        int total=0;
        total = (int) (item.getPrice()*item.getQuantity());
        return total;
    }
    
    public int sumTotal(List<Item> items){
        int order_total=0;
        for(Item item:items){
            order_total += item.getPrice() * item.getQuantity();
        }
        setCart_total(order_total);
        return order_total;    
    }
    
    public String viewCart(){   
        if(shoppingCart.isEmpty()){
            MessageHelper.addMessage("購物車裡面沒有東西");
            return null;
        }
        else if(!authBean.isLogin()){
            MessageHelper.addMessage("請先進行登入");
            return null;
        }
        else{
            return "/student/shoppingCart?faces-redirect=true";
        }
    }
    
    public String gotoCheckOutCart(){
        if(shoppingCart.isEmpty()){
            MessageHelper.addMessage("購物車裡面沒有東西");
            return null;
        }    
        return "checkOutCart?faces-redirect=true";
    }
    
    public String returnMain(){
        return "/main?faces-redirect=true";
    }
    
    public void clearCart(){
        shoppingCart.clear();
    }
    
    public String customerData(){
        if(authBean.isStudent()){
            Users users = usersPool.findByUserId(authBean.getPrincipalName());
            orderProcessingBean.getSalesOrder().setCustomerName(users.getStudent().getName());
            orderProcessingBean.getSalesOrder().setEmail(users.getStudent().getEmail());
            orderProcessingBean.getSalesOrder().setPhoneNumber(users.getStudent().getPhoneNumber());
            return "customerInformation?faces-redirect=true";
        }
        else{
            orderProcessingBean.getSalesOrder().setCustomerName("test");
            orderProcessingBean.getSalesOrder().setEmail("test");
            orderProcessingBean.getSalesOrder().setPhoneNumber("test");
            return "customerInformation?faces-redirect=true";
        }
    }
    
}
