/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.ItemFacade;
import ejb.SupplierFacade;
import ejb.UsersFacade;
import entities.Item;
import entities.Supplier;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.http.Part;
import security.AuthBean;
//import jsfbeans.mockData.MealPool;
//import jsfbeans.mockData.SupplierPool;
import util.filetool.FileUtil;
import util.order.MessageHelper;

/**
 * used facelet page : supplierManage.xhtml
 *
 * @author a19990366@gmail.com
 */
@Named(value = "mealManageBean")
@SessionScoped
public class MealManageBean implements Serializable {

    private Supplier currentSupplier;
    private Item createItem;
    private Part uploadPart;
    private String outputOSPath;
    private List<Supplier> supplierlist;
    private List<Item> displayItems;
    private String search;
    @EJB
    private ItemFacade mealPool;
    @EJB
    private SupplierFacade supplierStorage;
    @EJB
    private UsersFacade usersPool;
    @Inject
    private AuthBean authBean;
    
    /**
     * Creates a new instance of MealManageBean
     */
    public MealManageBean() {
    }

    @PostConstruct
    public void init() {
        createItem = new Item();
        supplierlist = supplierStorage.findAll();
    }

    public Supplier getCurrentSupplier() {
        return currentSupplier;
    }

    public void setCurrentSupplier(Supplier currentlySupplier) {
        this.currentSupplier = currentlySupplier;
    }

    public Item getCreateItem() {
        return createItem;
    }

    public void setCreateItem(Item createItem) {
        this.createItem = createItem;
    }

    public Part getUploadPart() {
        return uploadPart;
    }

    public void setUploadPart(Part uploadPart) {
        this.uploadPart = uploadPart;
    }

    public ItemFacade getMealPool() {
        return mealPool;
    }

    public void setMealPool(ItemFacade mealPool) {
        this.mealPool = mealPool;
    }

    public SupplierFacade getSupplierStorage() {
        return supplierStorage;
    }

    public void setSupplierStorage(SupplierFacade supplierStorage) {
        this.supplierStorage = supplierStorage;
    }

    public List<Supplier> getSupplierlist() {
        return supplierlist;
    }

    public void setSupplierlist(List<Supplier> supplierlist) {
        this.supplierlist = supplierlist;
    }

    public List<Item> getDisplayItems() {
        return displayItems;
    }

    public void setDisplayItems(List<Item> displayItems) {
        this.displayItems = displayItems;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
    
    public String uploadAction() {
        if (uploadPart == null) {
            MessageHelper.addMessage("請上傳圖片!");
            return null;
        } // make the output filename to OS file system.
        else {
//            this.outputOSPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
//            this.outputOSPath += File.separator + "resources" + File.separator + "img";
            this.outputOSPath = FileUtil.getImgOSLocation();
            System.out.printf("Web Root OS path %s", outputOSPath);
            
//            String tgtfilename = outputOSPath + "\\" + uploadPart.getSubmittedFileName();
            String tgtfilename = outputOSPath + File.separator + uploadPart.getSubmittedFileName();
            //Get the input string from the Part Object
            try (InputStream is = uploadPart.getInputStream()) {
                // Create a File instance to get the abstract path.
                File tgtFile = new File(tgtfilename);
                // Write the input stream to file
                Files.copy(is, tgtFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
                System.out.printf("Target file full name %s", tgtFile.toString());

            } catch (IOException e) {
                e.printStackTrace();
            }
//            createItem.setId(mealPool.count());
            createItem.setImgName(uploadPart.getSubmittedFileName());
            createItem.setSupplier(currentSupplier);
            mealPool.createItem(createItem);
            MessageHelper.addMessage("上傳成功!");
            return "supplierManage?faces-redirect=true";
        }
    }
    
    public String goAddMealPageAction(){
        // Create a new item to storing inputted data.
        this.createItem = new Item();
        return "addMeal?faces-redirect=true";
    }
    
    public String delMeal(Item item){
        mealPool.delete(item.getId());
        MessageHelper.addMessage("刪除成功!");
        return "delMeal?faces-redirect=true";
    }
    
    public String goSupplierManage(){
        currentSupplier = usersPool.findByUserId(authBean.getPrincipalName()).getSupplier();
        displayItems = mealPool.findByRestaurant(currentSupplier.getSupplierNo());
        return "/supplier/supplierManage?faces-redirect=true";
    }
    
    public String adminGoSupplierManage(){
        displayItems = mealPool.findAll();
        return "/supplier/supplierManage?faces-redirect=true";
    }

    public String itemfilter(){
        displayItems = mealPool.findByKeywordAndRestaurant(search, currentSupplier.getSupplierNo());
        return "/supplier/supplierManage?faces-redirect=true";
    }
    
}
