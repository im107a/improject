/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.SalesOrderFacade;
import ejb.SupplierFacade;
import ejb.WorkorderFacade;
import entities.OrderLine;
import entities.SalesOrder;
import entities.WorkOrder;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Inject;
//import jsfbeans.mockData.SalesOrderPool;
//import jsfbeans.mockData.SupplierPool;
//import jsfbeans.mockData.WorkOrderPool;
import util.order.SalesOrderHelper;
import util.order.WorkOrderHelper;

/**
 * Handle the page to create a sale order.
 * used facelet page : customerInformation.xhtml produceOrder.xhtml
 * @author hychen39@gm.cyut.edu.tw
 * @since 2018/11/6
 */
@Named(value = "orderProcessingBean")
@SessionScoped
public class OrderProcessingBean implements Serializable {

    private List<WorkOrder> workOrders;
    private List<OrderLine> orderLines;
    private SalesOrder salesOrder;
    @EJB
    private SupplierFacade supplierStorage;
    @EJB
    private WorkorderFacade workOrderStorage;
    @EJB
    private SalesOrderFacade salesOrderPoolStorage;
    
    @Inject
    private CartBean cartBean;
    /**
     * Creates a new instance of OrderProcessingBean
     */
    public OrderProcessingBean() {
    }
    
    @PostConstruct
    public void init(){
        // Create Sales Order
        salesOrder = new SalesOrder();
    }
    
    public List<OrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<OrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public CartBean getCartBean() {
        return cartBean;
    }

    public void setCartBean(CartBean cartBean) {
        this.cartBean = cartBean;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }
    
    public List<WorkOrder> getWorkOrders() {
        return workOrders;
    }

    public void setWorkOrders(List<WorkOrder> workOrders) {
        this.workOrders = workOrders;
    }
    
    public String produceOrder(){
        //抓取上一份訂單編號
        if(salesOrderPoolStorage.lastOrderNo()!=null){
            SalesOrderHelper.setLastOrderNo((long)salesOrderPoolStorage.lastOrderNo());
        }
        
        // Set Sales Order
        orderLines = SalesOrderHelper.createOrderLine(cartBean.getShoppingCart());
        salesOrder = SalesOrderHelper.setOrder(salesOrder,orderLines);
//        SalesOrderHelper.setOrderLineSoNo(orderLines, salesOrder);

        //加進so資料庫
        salesOrderPoolStorage.create(salesOrder);
        
        //建立工單
        workOrders = WorkOrderHelper.splitOrder(salesOrder, supplierStorage.findAll());
        //加進wo資料庫
        for(WorkOrder wo:workOrders){
            workOrderStorage.create(wo);
        }
        WorkOrderHelper.setSerial(0);
        //清除購物車內所有商品
        cartBean.clearCart();
        
        return "produceOrder?faces-redirect=true";
    }
    
}
