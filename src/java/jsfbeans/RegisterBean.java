/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.UsersFacade;
import ejb.Users_GroupsFacade;
import entities.Student;
import entities.Supplier;
import entities.Users;
import entities.Users_Groups;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import util.order.MessageHelper;

/**
 *
 * @author erus
 */
@Named(value = "registerBean")
@SessionScoped
public class RegisterBean implements Serializable {

    private Users users;
    private Student student;
    private Supplier supplier;
    @EJB
    private UsersFacade usersStorage;
    @EJB
    private Users_GroupsFacade ugStorage;
    
    /**
     * Creates a new instance of registerBean
     */
    public RegisterBean() {
    }
    
    @PostConstruct
    public void init() {
        //create new users
        users = new Users();
        student = new Student();
        supplier = new Supplier();
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public UsersFacade getUsersStorage() {
        return usersStorage;
    }

    public void setUsersStorage(UsersFacade usersStorage) {
        this.usersStorage = usersStorage;
    }
    
    public String signUpPage(){
        users = new Users();
        student = new Student();
        return "/auth/signUp?faces-redirect=true";
    }
    
    public String signUp(){
        student.setEmail(student.getEmail()+"@gm.cyut.edu.tw");
        users.setStudent(student);
        if(usersStorage.userIdExist(users)){
            MessageHelper.addMessage("帳號已存在!");
            student.setEmail("");
            return null;
        }
        else if(usersStorage.userEmailExist(users)){
            student.setEmail("");
            MessageHelper.addMessage("e-mail已被註冊!");
            return null;
        }
        else{
            usersStorage.createUsers(users);
            Users_Groups ug = new Users_Groups("student", users.getUserId());
            ugStorage.createUsers_Groups(ug);
            return "success?faces-redirect=true";
        }
    }
    
    public String supplierSignUpPage(){
        supplier = new Supplier();
        return "/admin/supplierSignUp?faces-redirect=true";
    }
    
    public String supplierSignUp(){
        if(usersStorage.userIdExist(users)){
            MessageHelper.addMessage("帳號已存在!");
            return null;
        }
        else{
            users.setSupplier(supplier);
            usersStorage.createUsers(users);
            Users_Groups ug = new Users_Groups("supplier", users.getUserId());
            ugStorage.createUsers_Groups(ug);
            return "/auth/success?faces-redirect=true";
        }
    }
}
