/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.SalesOrderFacade;
import entities.SalesOrder;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
//import jsfbeans.mockData.SalesOrderPool;

/**
 *
 * @author erus
 */
@Named(value = "salesOrderManager")
@SessionScoped
public class SalesOrderManager implements Serializable {

    private SalesOrder currentOrder;
    private List<SalesOrder> salesOrderList; //訂單
    
    @EJB
    private SalesOrderFacade salesOrderStorage;
    
    @PostConstruct
    public void init(){
        salesOrderList = salesOrderStorage.findAll();
    }
    
    public SalesOrderManager() {
    }
    
    public String checkDetail(SalesOrder salesOrder){
        currentOrder = salesOrder;
        return "salesOrderDetail?faces-redirect=true";
    }
    
    public SalesOrder getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(SalesOrder currentOrder) {
        this.currentOrder = currentOrder;
    }

    public List<SalesOrder> getSalesOrderList() {
        return salesOrderList;
    }

    public void setSalesOrderList(List<SalesOrder> salesOrderList) {
        this.salesOrderList = salesOrderList;
    }

    public SalesOrderFacade getSalesOrderStorage() {
        return salesOrderStorage;
    }

    public void setSalesOrderStorage(SalesOrderFacade salesOrderStorage) {
        this.salesOrderStorage = salesOrderStorage;
    }
    
    public String salesOrderFilter(String State){
        if(!State.equals("全部")){
            salesOrderList = salesOrderStorage.findByState(State);
            return "/admin/salesOrderList?faces-redirect=true";
        }
        else{
            salesOrderList = salesOrderStorage.findByDESC();
            return "/admin/salesOrderList?faces-redirect=true";
        }
    }
}
