/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.ItemFacade;
import ejb.SupplierFacade;
import entities.Item;
import entities.Supplier;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import util.order.MessageHelper;

/**
 *
 * @author a1999
 */
@Named(value = "searchBean")
@SessionScoped
public class SearchBean implements Serializable {

    private String title = "搜尋結果";
    private String search;
    private List<Item> searchResult;
    @EJB
    private ItemFacade mealPool;
    @EJB
    private SupplierFacade supplierPool;
    
    public SearchBean() {
    }
    
    @PostConstruct
    public void init() {
        searchResult = new ArrayList<>();
        search = new String();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public List<Item> getSearchResult() {
        return searchResult;
    }

    public void setSearchResult(List<Item> searchResult) {
        this.searchResult = searchResult;
    }
    
    public String search(){
        searchResult = mealPool.findByKeyword(search);
        title = "搜尋結果";
        return "/search.xhtml?faces-redirect=true";
    }
    
    public String supplierFilter(String name){
        Supplier supplier = supplierPool.findByName(name);
        searchResult = mealPool.findByRestaurant(supplier.getSupplierNo());
        title = supplier.getName();
        return "/search.xhtml?faces-redirect=true";
    }
     
}
