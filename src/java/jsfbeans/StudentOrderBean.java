/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.SalesOrderFacade;
import ejb.UsersFacade;
import entities.SalesOrder;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import security.AuthBean;

/**
 *
 * @author a1999
 */
@Named(value = "studentOrderBean")
@SessionScoped
public class StudentOrderBean implements Serializable{
    
    private List<SalesOrder> salesOrderList; //工單
    private SalesOrder currentSalesOrder;
    
    @EJB
    private SalesOrderFacade salesOrderPool;
    @EJB
    private UsersFacade usersPool;
    
    @Inject
    private AuthBean authBean;
    public StudentOrderBean() {
    }

    public List<SalesOrder> getSalesOrderList() {
        return salesOrderList;
    }

    public void setSalesOrderList(List<SalesOrder> salesOrderList) {
        this.salesOrderList = salesOrderList;
    }

    public SalesOrder getCurrentSalesOrder() {
        return currentSalesOrder;
    }

    public void setCurrentSalesOrder(SalesOrder currentSalesOrder) {
        this.currentSalesOrder = currentSalesOrder;
    }
    
    public String showSalesOrderList(SalesOrder salesOrder){
        currentSalesOrder = salesOrder;
        return "/student/salesOrderDetail?faces-redirect=true";
    }
    
    public String salesOrderFilter(String State){
        if(!State.equals("全部")){
            salesOrderList = salesOrderPool.findByStudentEmailAndState(usersPool.findByUserId(authBean.getPrincipalName()).getStudent().getEmail(), State);
            return "/student/salesOrderList?faces-redirect=true";
        }
        else{
            salesOrderList = salesOrderPool.findByEmail(usersPool.findByUserId(authBean.getPrincipalName()).getStudent().getEmail());
            return "/student/salesOrderList?faces-redirect=true";
        }
    }
    
}
