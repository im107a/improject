/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.SalesOrderFacade;
import ejb.SupplierFacade;
import ejb.UsersFacade;
import ejb.WorkorderFacade;
import entities.OrderLine;
import entities.SalesOrder;
import entities.WorkOrder;
import entities.WorkOrderLine;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import security.AuthBean;
import util.order.MessageHelper;
//import jsfbeans.mockData.SupplierPool;
//import jsfbeans.mockData.WorkOrderPool;

/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "supplierOrderListBean")
@SessionScoped
public class SupplierOrderListBean implements Serializable{
    
    private List<WorkOrder> workOrderList; //工單
    private WorkOrder currentWorkOrder;
    
    @EJB
    private SupplierFacade supplierStorage;
    @EJB
    private WorkorderFacade workOrderStorage;
    @EJB
    private SalesOrderFacade salesOrderStorage;
    @EJB
    private UsersFacade usersStorage;
    
    @Inject
    private AuthBean authBean;
    
    @PostConstruct
    public void init(){
        workOrderList = new ArrayList<>();
//        selectedSupplier = supplierStorage.find(1);
    }

    public SupplierOrderListBean() {
    }

    public List<WorkOrder> getWorkOrderList() {
        return workOrderList;
    }

    public void setWorkOrderList(List<WorkOrder> workOrderList) {
        this.workOrderList = workOrderList;
    }

    public WorkOrder getCurrentWorkOrder() {
        return currentWorkOrder;
    }

    public void setCurrentWorkOrder(WorkOrder currentWorkOrder) {
        this.currentWorkOrder = currentWorkOrder;
    }

    public SupplierFacade getSupplierStorage() {
        return supplierStorage;
    }

    public void setSupplierStorage(SupplierFacade supplierStorage) {
        this.supplierStorage = supplierStorage;
    }

    public WorkorderFacade getWorkOrderStorage() {
        return workOrderStorage;
    }

    public void setWorkOrderStorage(WorkorderFacade workOrderStorage) {
        this.workOrderStorage = workOrderStorage;
    }
    
    public String showSupplierOrderList(WorkOrder workOrder){
        currentWorkOrder = workOrder;
        return "supplierOrderDetail?faces-redirect=true";
    }
    
    public String workOrderFilter(String State){
        if(!State.equals("全部")){
            workOrderList = workOrderStorage.findBySupplierAndState(usersStorage.findByUserId(authBean.getPrincipalName()).getSupplier().getSupplierNo(), State);
            return "/supplier/supplierOrderList?faces-redirect=true";
        }
        else{
            workOrderList = workOrderStorage.findBySupplier(usersStorage.findByUserId(authBean.getPrincipalName()).getSupplier().getSupplierNo());
            return "/supplier/supplierOrderList?faces-redirect=true";
        }
    }
    
    public void setOrderState(WorkOrder workOrder,String state){
        //設定工單狀態
        workOrder.setState(state);
        workOrderStorage.updateWorkOrder(workOrder.getWorkOrderNo(), workOrder);
        
        //對比工單設定訂單明細行狀態
        SalesOrder salesOrder = salesOrderStorage.findByWorkOrderNo(workOrder.getWorkOrderNo());
        for(WorkOrderLine workOrderLine:workOrder.getLines()){
            for(OrderLine orderLine:salesOrder.getLines()){
                if(orderLine.getItem().getId() == workOrderLine.getItem().getId()){
                    orderLine.setOrderState(state);
                }
            }
        }
        
        //對比訂單明細行狀態都完成更新訂單狀態
        Boolean salesOrderState = true;
        for(OrderLine orderLine:salesOrder.getLines()){
            if(!orderLine.getOrderState().equals("完成")){
                salesOrderState = false;
                break;
            }
        }
        if(salesOrderState == true){
            salesOrder.setSalesOrderState("完成");
        }
        salesOrderStorage.updateSalesOrder(salesOrder.getOrderNo(), salesOrder);
        MessageHelper.addMessage("訂單狀態更新完成");
    }
}
    
