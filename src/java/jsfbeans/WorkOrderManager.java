/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jsfbeans;

import ejb.WorkorderFacade;
import entities.WorkOrder;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
//import jsfbeans.mockData.WorkOrderPool;

/**
 * used facelet page : workOrderList.xhtml displayWorkOrder.xhtml orderList.xhtml
 * @author hychen39@gm.cyut.edu.tw
 */
@Named(value = "workOrderManager")
@SessionScoped
public class WorkOrderManager implements Serializable {
    private WorkOrder currentOrder;
    private List<WorkOrder> workOrderList; //工單    
    
    @EJB
    private WorkorderFacade workOrderStorage;
    
    @PostConstruct
    public void init(){
        workOrderList = workOrderStorage.findAll();
    }
    
    public WorkOrderManager() {
    }

    public WorkOrder getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(WorkOrder currentOrder) {
        this.currentOrder = currentOrder;
    }
    
    public List<WorkOrder> getWorkOrderList() {
        return workOrderList;
    }

    public void setWorkOrderList(List<WorkOrder> workOrderList) {
        this.workOrderList = workOrderList;
    }

    public WorkorderFacade getWorkOrderStorage() {
        return workOrderStorage;
    }

    public void setWorkOrderStorage(WorkorderFacade workOrderStorage) {
        this.workOrderStorage = workOrderStorage;
    }
    
    public String checkDetail(WorkOrder workorder){
        currentOrder = workorder;
        return "workOrderDetail?faces-redirect=true";
    }
    
    public String workOrderFilter(String State){
        if(!State.equals("全部")){
            workOrderList = workOrderStorage.findByState(State);
            return "/admin/workOrderList?faces-redirect=true";
        }
        else{
            workOrderList = workOrderStorage.findByDESC();
            return "/admin/workOrderList?faces-redirect=true";
        }
    }
    
}
