/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jsfbeans.converters;

import ejb.SupplierFacade;
import entities.Supplier;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.naming.InitialContext;
import javax.naming.NamingException;
//import jsfbeans.mockData.SupplierPool;

/**
 * Custom converter for {@link entities.Supplier}.
 * 
 * @author hychen39@gm.cyut.edu.tw
 * @since Dec 31, 2018
 */
@FacesConverter("supplierConverter")
public class SupplierConverter implements Converter{
    //@EJB
    private SupplierFacade supplierStorage;
    
    /**
     * Convert from the supplier id to a {@code Supplier} instance.
     * @param context
     * @param component
     * @param value Supplier ID as the String.
     * @return {@code Supplier} instance
     * @see entities.Supplier 
     */
    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        // Convert from the presentation to model tier.
        
        // get the supplierStorage EJB using the JNDI api
        // The JNDI name for the SupplierPool object.
        String ejbJNDIName = "java:global/improject/SupplierFacade";
        InitialContext ic;
        try {
            ic = new InitialContext();
            supplierStorage = (SupplierFacade) ic.lookup(ejbJNDIName);
            System.out.println("SupplierStorage: " + supplierStorage.toString());
        } catch (NamingException ex) {
            Logger.getLogger(SupplierConverter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        int supplierID = -1;
        
        try{
            supplierID = Integer.valueOf(value);
        } catch (NumberFormatException e){
            System.out.println("SupplierConverter: the supplier id cannot convert to int value");
            e.printStackTrace();
        }
        Supplier supplier = supplierStorage.find(supplierID);
        System.out.printf("**SupplierConverter**: Supplier name: %s \n", supplier.getName());
        return supplier;
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Convert the {@code Supplier} instance to its supplier id.
     * @param context
     * @param component
     * @param value
     * @return Supplier ID
     */
    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        // Convert from the model to presentation tier.
        Supplier supplier = (Supplier) value;
       
        return String.valueOf(supplier.getSupplierNo());
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
