///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package jsfbeans.mockData;
//
//import entities.Item;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.ejb.EJB;
//import javax.ejb.Singleton;
//
//
///**
// * 模擬餐點的資料
// * @author a19990366@gmail.com
// */
//@Singleton
//public class MealPool {
//    private Map<Integer, Item> repository;
//    private Integer lastIndex;
//    @EJB
//    private SupplierPool supplierStorage;
//    
//    public MealPool() {
//        
//        repository = new HashMap<>();
//        lastIndex = 1;
//    }
//    
//    @PostConstruct
//    public void init(){
//        this.create(new Item(1,"龍貓便當",80,"img1.jpg",supplierStorage.query(1)));
//        this.create(new Item(2,"拉拉熊便當",90,"img2.jpg",supplierStorage.query(1)));
//        this.create(new Item(3,"貓咪便當",90,"img3.jpg",supplierStorage.query(2)));
//        this.create(new Item(4,"柴犬便當",95,"img4.jpg",supplierStorage.query(2)));
//        this.create(new Item(5,"憤怒鳥便當",70,"img5.jpg",supplierStorage.query(3)));
//        this.create(new Item(6,"花媽便當",85,"img6.jpg",supplierStorage.query(3)));
//    }
//    
//    public int create(Item item){
//        int currentIdx = lastIndex;
//        item.setId(currentIdx);
//        repository.put(currentIdx, item);
//        lastIndex++;
//        return currentIdx;
//    }
//    
//    public Item query(Integer id){
//        if (repository.containsKey(id)){
//            return repository.get(id);
//        } else
//        return null;
//    }
//    
//    public List<Item> queryAll(){
//        List<Item> results = new ArrayList<>();
//        List<Integer> keys = new ArrayList<>();
//        for(int key : repository.keySet()){
//            keys.add(key);
//        }
//        Collections.sort(keys);
//        for(int key : keys){
//            results.add(repository.get(key));
//        }
//        return results;
//    }
//    
//    public int update(Integer id,Item item){
//        if (repository.containsKey(id)){
//            repository.put(id, item);
//        }
//        return id;
//    }
//    
//     public int delete(Integer id){
//        if (repository.containsKey(id)){
//            repository.remove(id);
//        }
//        return id;
//    }
//    
//}
