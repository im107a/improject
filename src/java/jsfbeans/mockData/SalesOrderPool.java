///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package jsfbeans.mockData;
//
//import entities.SalesOrder;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.ejb.Singleton;
//
///**
// * 模擬訂單的資料
// * @author erus
// */
//@Singleton
//public class SalesOrderPool {
//    private Map<Long, SalesOrder> salesOrderRepository;
//    /**
//     * Creates a new instance of Suppliers
//     */
//    @PostConstruct
//    public void init(){
//        
//    }
//    
//    public SalesOrderPool() {
//        salesOrderRepository = new HashMap<>();
//    }
//    
//    public long create(SalesOrder salesOrder){
//        salesOrderRepository.put(salesOrder.getOrderNo(), salesOrder);
//        return salesOrder.getOrderNo();
//    }
//    
//    public SalesOrder query(Long id){
//        if (salesOrderRepository.containsKey(id)){
//            return salesOrderRepository.get(id);
//        } else
//        return null;
//    }
//    
//    public List<SalesOrder> queryAll(){
//        List<SalesOrder> results = new ArrayList<>();
//        List<Long> keys = new ArrayList<>();
//        for(long key : salesOrderRepository.keySet()){
//            keys.add(key);
//        }
//        Collections.sort(keys);
//        Collections.reverse(keys);
//        for(long key : keys){
//            results.add(salesOrderRepository.get(key));
//        }
//        return results;
//    }
//}
