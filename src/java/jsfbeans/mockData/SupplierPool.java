///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package jsfbeans.mockData;
//
//
//import entities.Supplier;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.ejb.Singleton;
//
///**
// * 模擬供應商的資料
// * @author a19990366@gmail.com
// */
//@Singleton
//public class SupplierPool  {
//    private Map<Integer, Supplier> supplierRepository;
//    private Integer lastIndex;
//    /**
//     * Creates a new instance of Suppliers
//     */
//    @PostConstruct
//    public void init(){
//        this.create(new Supplier(1,"大甲鵝",1));
//        this.create(new Supplier(2,"金鑽",2));
//        this.create(new Supplier(3,"古早味",3));
//    }
//    
//    public SupplierPool() {
//        supplierRepository = new HashMap<>();
//        lastIndex = 1;
//    }
//    
//    public int create(Supplier supplier){
//        int currentIdx = lastIndex;
//        supplierRepository.put(currentIdx, supplier);
//        lastIndex++;
//        return currentIdx;
//    }
//    
//    public Supplier query(Integer id){
//        if (supplierRepository.containsKey(id)){
//            return supplierRepository.get(id);
//        } else
//        return null;
//    }
//    
//    public List<Supplier> queryAll(){
//        List<Supplier> results = new ArrayList<>();
//        for(int i=1;i<=supplierRepository.size();i++){
//            results.add(supplierRepository.get(i));
//        }
//        return results;
//    }
//    
//}
