///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package jsfbeans.mockData;
//
//
//import entities.WorkOrder;
//import java.util.ArrayList;
//import java.util.Collections;
//import java.util.Comparator;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import javax.annotation.PostConstruct;
//import javax.ejb.Singleton;
//
///**
// * 模擬工單的資料
// * @author a19990366@gmail.com
// */
//@Singleton
//public class WorkOrderPool  {
//    private Map<Long, WorkOrder> workOrderRepository;
//    /**
//     * Creates a new instance of Suppliers
//     */
//    @PostConstruct
//    public void init(){
//        
//    }
//    
//    public WorkOrderPool() {
//        workOrderRepository = new HashMap<>();
//    }
//    
//    public long create(WorkOrder workOrder){
//        workOrderRepository.put(workOrder.getWorkOrderNo(), workOrder);
//        return workOrder.getWorkOrderNo();
//    }
//    
//    public WorkOrder query(Long id){
//        if (workOrderRepository.containsKey(id)){
//            return workOrderRepository.get(id);
//        } else
//        return null;
//    }
//    
//    public List<WorkOrder> queryAll(){
//        List<WorkOrder> results = new ArrayList<>();
//        List<Long> keys = new ArrayList<>();
//        for(long key : workOrderRepository.keySet()){
//            keys.add(key);
//        }
//        Collections.sort(keys);
//        Collections.reverse(keys);
//        for(long key : keys){
//            results.add(workOrderRepository.get(key));
//        }
//        return results;
//    }
//    
//}
