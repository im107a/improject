/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest_resources;

import ejb.ItemFacade;
import entities.Item;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.core.UriInfo;
//import jsfbeans.mockData.MealPool;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

/**
 * 
 * @author a19990366@gmail.com
 */
@Stateless
@Path("/dishes")
public class DishesResources {
    @EJB
    private ItemFacade dishStorage;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItems(){
        List<Item> allDishes = dishStorage.findAll();
          return Response
                .ok()
                .entity(allDishes)
                .build();
    }
    
    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getItems(@PathParam("id") int id){
        Item item = dishStorage.find(id);
        if (item != null){
            return Response.ok()
                   .entity(item)  // convert from object to json by JAX-RS
                   .build();
        }
        return Response.ok().entity("Cannot find the dish").build();
    }
    
    @GET
    @Path("/search/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchByKeyword(@PathParam("name") String name){
        List<Item> result = dishStorage.findByKeyword(name);
        if (result != null){
            return Response.ok()
                   .entity(result)
                   .build();
        }
        return Response.ok().entity("Cannot find the dish").build();
    }
    
    @POST
    @Path("post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addDish(Item item){
        int idx = dishStorage.createItem(item);
        Item createdItem = dishStorage.find(idx);
        String value = String.format("ID: %s; Name: %s; Price: %s; Supplier: %s", 
                createdItem.getId(), createdItem.getName(), createdItem.getPrice(), createdItem.getSupplier().getName());
        return Response.ok(value).build();
    }
    
    @PUT
    @Path("/put/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateDish(@PathParam("id") int id,Item item){
        int idx = dishStorage.updateItem(id,item);
        Item updatedItem = dishStorage.find(idx);
        String value = String.format("ID: %s; Name: %s; Price: %s; Supplier: %s", 
                updatedItem.getId(), updatedItem.getName(), updatedItem.getPrice(), updatedItem.getSupplier().getName());
        return Response.ok(value).build();
    }
    
    @DELETE
    @Path("/delete/{id}")
    public Response removeDish(@PathParam("id") int id){
        dishStorage.delete(id);
        return Response.ok("ID:" + id + " dish has been deleted").build();
    }
    
}
