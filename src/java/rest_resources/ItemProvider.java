/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package rest_resources;

import ejb.SupplierFacade;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.Consumes;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.Provider;
import entities.Item;
import javax.ejb.EJB;
//import jsfbeans.mockData.SupplierPool;

/**
 * Dish provider to read HTTP message body and return the {@link Dish} object.
 * @author hychen39@gm.cyut.edu.tw
 * @since May 6, 2019
 */
@Provider
@Consumes(MediaType.APPLICATION_JSON)
public class ItemProvider implements MessageBodyReader<Item>{
    @EJB
    private SupplierFacade supplierStorage;
    
    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return type == Item.class;
    }

    @Override
    public Item readFrom(Class<Item> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
        InputStreamReader entityStreamReader = new InputStreamReader(entityStream);
        // ToDo
        JsonReader jsonReader ;
        jsonReader = Json.createReader(entityStreamReader);
        JsonObject jsonObject = jsonReader.readObject();
        
        Item item = new Item();
        item.setId(jsonObject.getInt("id"));
        item.setName(jsonObject.getString("name", "new Dish"));
        item.setPrice(jsonObject.getInt("price"));
        item.setImgName(jsonObject.getString("imgName"));
        item.setSupplier(supplierStorage.find(jsonObject.getJsonObject("supplier").getInt("supplierNo")));
        
        System.out.printf("Dish name: %s", item.getName());
        
        return item;
    }

}
