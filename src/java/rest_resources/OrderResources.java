/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest_resources;

import ejb.ItemFacade;
import ejb.SalesOrderFacade;
import ejb.SupplierFacade;
import ejb.WorkorderFacade;
import entities.OrderLine;
import entities.SalesOrder;
import entities.WorkOrder;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
//import jsfbeans.mockData.MealPool;
//import jsfbeans.mockData.SalesOrderPool;
//import jsfbeans.mockData.SupplierPool;
//import jsfbeans.mockData.WorkOrderPool;
import util.order.SalesOrderHelper;
import util.order.WorkOrderHelper;

/**
 *
 * @author erus
 */
@Stateless
@Path("/orders")
public class OrderResources {
    @EJB
    private ItemFacade dishesStorage;
    @EJB
    private SupplierFacade supplierStorage;
    @EJB
    private WorkorderFacade woStorage;
    @EJB
    private SalesOrderFacade soStorage;
    
    //wo.get
    @GET
    @Path("/wo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkOrders(){
        System.out.println(String.format("orderStorage: %s", woStorage));
        List<WorkOrder> wos = woStorage.findByDESC();
        System.out.println(String.format("Order size: %s",  wos.size()));
        //ToDO: Cause the following exception
        //Severe:   Generating incomplete JSON
        //Warning:   StandardWrapperValve[rest_resources.AppConfig]: Servlet.service() for servlet rest_resources.AppConfig threw exception
        //java.lang.StackOverflowError
        // Possible cause: WorkOrder object cannot be converted to JSON.
        return Response
                .ok()
                .entity(wos)
                .build();
    }
    //curl  http://localhost:8080/improject/rest/orders/wo
    
    //wo.getId
    @GET
    @Path("/wo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getWorkOrder(@PathParam("id") long id){
        WorkOrder workorder = woStorage.find(id);
        if (workorder != null){
            return Response.ok()
                   .entity(workorder)  // convert from object to json by JAX-RS
                   .build();
        }
        return Response.ok().entity("Cannot find the workorder").build();
    }
    //curl  http://localhost:8080/improject/rest/orders/wo/x x=工單id
    
    //so.get
    @GET
    @Path("/so")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSalesOrders(){
        System.out.println(String.format("orderStorage: %s", soStorage));
        List<SalesOrder> sos = soStorage.findByDESC();
        System.out.println(String.format("Order size: %s",  sos.size()));
        return Response
                .ok()
                .entity(sos)
                .build();
    }
    //curl  http://localhost:8080/improject/rest/orders/so
     
    //so.getID
    @GET
    @Path("/so/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSalesOrder(@PathParam("id") long id){
        SalesOrder salesOrder = soStorage.find(id);
        if (salesOrder != null){
            return Response.ok()
                   .entity(salesOrder)  // convert from object to json by JAX-RS
                   .build();
        }
        return Response.ok().entity("Cannot find the salesorder").build();
    }
    //curl  http://localhost:8080/improject/rest/orders/so/x x=訂單id
    
    //so.post
    @POST
    @Path("/so/post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addSalesOrder(SalesOrder salesorder){
        for(OrderLine ol : salesorder.getLines()){
            int quantity = ol.getItem().getQuantity();
//            ol.setSalesOrderID(salesorder.getOrderNo());
            ol.setItem(dishesStorage.find(ol.getItem().getId()));
            ol.setQuantity(quantity);
            ol.getItem().setQuantity(quantity);
        }
        salesorder = SalesOrderHelper.setOrder(salesorder,salesorder.getLines());
        //建立工單
        List<WorkOrder> workOrders = WorkOrderHelper.splitOrder(salesorder, supplierStorage.findAll());
        //加進wo資料庫
        for(WorkOrder wo:workOrders){
            woStorage.create(wo);
        }
        WorkOrderHelper.setSerial(0);
        long idx = soStorage.createSalesOrder(salesorder);
        SalesOrder createdSalesOrder = soStorage.find(idx);
        String value = String.format("orderTime: %s; customerName: %s; email: %s; phoneNumber: %s; OrderNo: %s; total: %s;",
                createdSalesOrder.getOrderTime(),createdSalesOrder.getCustomerName(),createdSalesOrder.getEmail(),createdSalesOrder.getPhoneNumber(),
                createdSalesOrder.getOrderNo(),createdSalesOrder.getTotal());
        return Response.ok(value).build();
    }
    //curl -i  -X POST --header "content-type: application/json; charset=utf-8" --data '{"customerName":"777","email":"777@gmail.com","lines":[{"item":{"id":1,"quantity":1}},{"item":{"id":2,"quantity":1}},{"item":{"id":3,"quantity":1}},{"item":{"id":4,"quantity":1}},{"item":{"id":5,"quantity":1}},{"item":{"id":6,"quantity":1}}],"phoneNumber":"0987654321"}' http://localhost:8080/improject/rest/orders/so/post
}
