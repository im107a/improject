/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest_resources;

import ejb.UsersFacade;
import ejb.Users_GroupsFacade;
import entities.Users;
import entities.Users_Groups;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author a1999
 */
@Stateless
@Path("/users")
public class UsersResources {
    
    @EJB
    private UsersFacade usersPool;
    @EJB
    private Users_GroupsFacade ugStorage;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUsers(){
        List<Users> allUsers = usersPool.findAll();
          return Response
                .ok()
                .entity(allUsers)
                .build();
    }
    
    @POST
    @Path("post")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addUser(Users users){
        if(usersPool.userIdExist(users)){
            return Response.ok("帳號已被註冊").build();
        }
        if(usersPool.userEmailExist(users)){
            return Response.ok("email已被註冊").build();
        }
        else{
            String userId = usersPool.createUsers(users);
            Users createduser = usersPool.find(userId);
            Users_Groups ug = new Users_Groups("student", users.getUserId());
            ugStorage.createUsers_Groups(ug);
            String value = String.format("ID: %s; Name: %s; Email: %s; phoneNumber: %s", 
                    createduser.getUserId(), createduser.getStudent().getName(), createduser.getStudent().getEmail(), createduser.getStudent().getPhoneNumber());
            return Response.ok(value).build();
        }
        
    }
    //curl -i  -X POST --header "content-type: application/json; charset=utf-8"  --data '{"id":"777","password":"7777","student":{"email":"s10514777@gm.cyut.edu.tw","name":"777","phoneNumber":"0987654321"},"userId":"777"}'  http://localhost:8080/improject/rest/users/post
}
