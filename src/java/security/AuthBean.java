/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package security;

import java.security.Principal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author erus
 */
@Named(value = "authBean")
@RequestScoped
public class AuthBean {
    
    
    @Inject 
    private Principal principal;
    @Inject
    private HttpServletRequest request;
    /**
     * Creates a new instance of authBean
     */
    public AuthBean() {
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }
    
    public String getPrincipalName() {
        return principal.getName();
    }
    
    public boolean isLogin(){
        String remoteUser  = request.getRemoteUser();
        return (remoteUser != null);
    }
    
    public String logout(){
        try{
            request.logout();
        }
        catch(ServletException ex){
            Logger.getLogger(AuthBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "/main?faces-redirect=true";
    }
    
    public boolean isAdmin(){
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.isUserInRole("admin");
    }
    
    public boolean isSupplier(){
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.isUserInRole("supplier");
    }
    
    public boolean isStudent(){
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        return context.isUserInRole("student");
    }
}
