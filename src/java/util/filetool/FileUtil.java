
package util.filetool;

import java.io.File;
import javax.faces.context.FacesContext;

/**
 * 檔案操作小工具
 * @author hychen39@gm.cyut.edu.tw
 * @since May 1, 2019
 */
public class FileUtil {
    /**
     * Return the OS location for saving the uploaded image.
     * @return 
     */
    static public String getImgOSLocation(){
        String outputOSPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
        outputOSPath += File.separator + "resources" + File.separator + "img";
        return outputOSPath;
    }
}
