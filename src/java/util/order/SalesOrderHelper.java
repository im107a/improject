/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.order;

import entities.Item;
import entities.OrderLine;
import entities.SalesOrder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * A helper class to generate sales order.
 * @author hychen39@gm.cyut.edu.tw
 * @since 2018/11/06
 */
public class SalesOrderHelper {
    
    // 訂單流水號
    private static long serial = 0 ;
    private static long lastOrderNo = 100001010001l;
    
    /**
     * Create a sales order given the shoppingCart
     * @param salesOrder
     * @param orderLines
     * @return {@link SalesOrder} object.
     */
    public static SalesOrder setOrder(SalesOrder salesOrder,List<OrderLine> orderLines){
        SalesOrder so = new SalesOrder();
        so.setCustomerName(salesOrder.getCustomerName());
        so.setPhoneNumber(salesOrder.getPhoneNumber());
        so.setEmail(salesOrder.getEmail());
        // set the order header
        so.setOrderNo(genOrderNo());
        // Get Time
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String formatDateTime = now.format(formatter);
        so.setOrderTime(formatDateTime);
        // Get Total
        int orderTotal = 0;
        for(OrderLine orderLine:orderLines){
            orderTotal += (orderLine.getItem().getPrice()*orderLine.getItem().getQuantity());
        }
        so.setTotal(orderTotal);
        so.setLines(orderLines);
        return so;
    }
    
    public static List<OrderLine> createOrderLine(List<Item> cart){
        List<OrderLine> lines = new ArrayList<>();
        for (Item item : cart) {
            OrderLine line = new OrderLine();
            line.setItem(item);
            line.setQuantity(item.getQuantity());
            lines.add(line);
        }
        return lines;
    }
    
//    public static void setOrderLineSoNo(List<OrderLine> ols,SalesOrder so){
//        for (OrderLine ol : ols) {
//            ol.setSalesOrderID(so.getOrderNo());
//        }
//    }

    /**
     * 訂單產生編號(規則:訂單日期+流水號後四碼)
     * 
     * @return orderNumber
     */
    public static long genOrderNo(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String nowdate = sdf.format(new Date());
        DecimalFormat df = new DecimalFormat("0000");
        String lastOrder = Long.toString(lastOrderNo);
        lastOrder = lastOrder.substring(0,8);
        if (lastOrder.equals(nowdate)){
            serial ++;
            long orderNumber = Long.parseLong(nowdate+df.format(serial));
            return orderNumber;
        }
        else{
            serial = 1;
            long orderNumber = Long.parseLong(nowdate+df.format(serial));
            return orderNumber;
        }
    }
    
    /**
     * 設定訂單編號(用於從資料庫抓出最後訂單並設定lastOrderNo和serial)
     * @param l 
     */
    public static void setLastOrderNo(long l) {
        lastOrderNo = l;
        serial = l % 10000;
    }
    
}
