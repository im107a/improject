/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util.order;

import entities.WorkOrderLine;
import entities.OrderLine;
import entities.SalesOrder;
import entities.Supplier;
import entities.WorkOrder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 工單處理
 * @author a19990366@gmail.com
 */
public class WorkOrderHelper {
    
    // 訂單流水號
    private static long serial = 0 ;
    
    /**
     * 建立工單
     * @param supplier 店家
     * @param workOrderLines
     * @param salesOrder
     * @return 工單
     */
    public static WorkOrder createWorkOrder(Supplier supplier,List<WorkOrderLine> workOrderLines,SalesOrder salesOrder){
        WorkOrder wo = new WorkOrder();
        //工單編號
        DecimalFormat df = new DecimalFormat("00");
        serial ++;
        long workOrderNo = Long.parseLong(salesOrder.getOrderNo()+df.format(serial));
        wo.setWorkOrderNo(workOrderNo);
        //工單總計
        int woTotal = 0;
        for(WorkOrderLine ol:workOrderLines){
            woTotal += (ol.getItem().getPrice()*ol.getItem().getQuantity());
//            ol.setWorkOrderID(workOrderNo);
        }
        wo.setWorkOrderTotal(woTotal);
        wo.setSupplier(supplier);
        wo.setLines(workOrderLines);
        wo.setOrderTime(salesOrder.getOrderTime());
        wo.setCustomerName(salesOrder.getCustomerName());
        wo.setPhoneNumber(salesOrder.getPhoneNumber());
        return wo;
    }
    
    /**
     * 拆工單並將工單列表傳回
     * @param salesOrder
     * @param suppliers
     * @return 工單列表
     */
    public static List<WorkOrder> splitOrder(SalesOrder salesOrder,List<Supplier> suppliers){
        List<WorkOrder> workOrders = new ArrayList<>();
        WorkOrder wo;
        for (Supplier supplier:suppliers){
            List<WorkOrderLine> lines = new ArrayList<>();
            for (OrderLine orderLine : salesOrder.getLines()) {
                if(orderLine.getItem().getSupplier().getSupplierNo()==supplier.getSupplierNo()){
                    WorkOrderLine workOrderLine = new WorkOrderLine();
                    workOrderLine.setItem(orderLine.getItem());
                    workOrderLine.setQuantity(orderLine.getQuantity());
                    lines.add(workOrderLine);
                }
            }
            if(!lines.isEmpty()){
                wo = createWorkOrder(supplier,lines,salesOrder);
                workOrders.add(wo);
            }
        }
        return workOrders;
    }
    
    public static long getSerial() {
        return serial;
    }

    public static void setSerial(long serial) {
        WorkOrderHelper.serial = serial;
    }
    
}
