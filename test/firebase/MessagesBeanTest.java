/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package firebase;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static junit.framework.Assert.assertFalse;
import org.junit.Test;


/**
 *
 * @author hychen39@gm.cyut.edu.tw
 */
public class MessagesBeanTest {
    
    public MessagesBeanTest() {
    }
    
    
    /**
     * Test of initBean method, of class messagesBean.
     */
    @Test
    public void testInitBean() {
        MessagesBean messagesBean = new MessagesBean();
        messagesBean.initBean();
        boolean initFlag = true;
        try {
            initFlag = messagesBean.initFirebase();
        } catch (IOException ex) {
            Logger.getLogger(MessagesBeanTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertFalse(initFlag);
    }

    /**
     * Test of initFirebase method, of class messagesBean.
     */
    @Test
    public void testInitFirebase() throws Exception {
    }

    /**
     * Test of notifyMealReady method, of class messagesBean.
     */
    @Test
    public void testNotifyMealReady() throws Exception {
    }

    /**
     * Test of messaging method, of class messagesBean.
     */
    @Test
    public void testMessaging() throws Exception {
    }

    /**
     * Test of notifyCloseWorkOrder method, of class messagesBean.
     */
    @Test
    public void testNotifyCloseWorkOrder() throws Exception {
    }
    
}
